PANDOC?=pandoc

DOCNAME=njet-ir
DOC=$(DOCNAME).md
BIB=$(DOCNAME).bib
TEX=$(DOCNAME).tex
WEB_DIR=public
PDF=$(WEB_DIR)/$(DOCNAME).pdf
WEB=$(WEB_DIR)/index.html
IMG=$(WEB_DIR)/img
TIKZ=$(IMG)/tikz
KATEX_V=0.12.0
SNIPS=code-examples/c2_hel_amp_2.cpp code-examples/s1_hel_amp_2.cpp

all: $(PDF) $(WEB)

.PHONY: clean pdf wipe cont cont-web web

$(TEX): $(DOC) $(BIB) header.tex
	$(PANDOC) -s --biblatex --bibliography=$(word 2, $^) --include-in-header=$(word 3, $^) -o $@ $<

test: $(TEX) $(BIB)
	latexmk -pdf -output-directory=$@ $<

pdf: $(PDF)
	evince $< &

web: $(WEB)
	firefox $< &

$(PDF): $(DOC) $(BIB) pdf.yml $(TIKZ) md4pdf.sh shared.yml $(SNIPS)
	./$(word 5, $^) $(DOC)
	$(PANDOC) --defaults=$(word 6, $^) --defaults=$(word 3, $^) -o $@ $(DOC).md4pdf
	-rm $(DOC).md4pdf

cont-pdf: $(DOC) $(BIB) pdf.yml tikz/*.tex shared.yml Makefile $(SNIPS) draw.tex
	ls $^ | entr make $(PDF)

$(WEB): $(DOC) $(BIB) html.yml $(TIKZ) $(WEB_DIR)/katex md4html.sh shared.yml $(SNIPS)
	mkdir -p $(WEB_DIR)
	./$(word 6, $^) $(DOC)
	$(PANDOC) --defaults=$(word 7, $^) --defaults=$(word 3, $^) -o $@ $(DOC).md4html
	-rm $(DOC).md4html
	sed -i "s/\(fleqn: \)/trust: true, \1/g" $(WEB)

$(WEB_DIR)/katex:
	mkdir -p $(WEB_DIR)
	wget https://github.com/KaTeX/KaTeX/releases/download/v$(KATEX_V)/katex.tar.gz
	tar --extract --gzip --file=katex.tar.gz --directory=$(WEB_DIR)
	-rm katex.tar.gz

cont-web: $(DOC) $(BIB) html.yml tikz/*.tex shared.yml Makefile $(SNIPS) draw.tex
	ls $^ | entr -s 'make $(WEB)'

$(TIKZ): draw.tex tikz/
	mkdir -p $@
	pdflatex -shell-escape -draftmode $<
	for f in $@/*.pdf; do pdf2svg "$$f" "$${f%.pdf}.svg"; done

clean:
	-rm $(TEX)
	-rm -r test
	-rm *.auxlock
	-rm *.aux
	-rm *.log

wipe: clean
	-rm $(PDF)
	-rm $(WEB)
	-rm -r $(IMG)/tikz
