#!/usr/bin/env sh

file=$1.md4html

cp $1 $file
perl -i -p \
    -e "s|(\\\includegraphics)\{(.+?)\}|\\\;\\\raisebox{-3.15em}{\\\$\1\[height=7em\]{img/tikz/\2.svg}\\\$}\\\;|gm" \
    $file
