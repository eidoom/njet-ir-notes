#!/usr/bin/env sh

file=$1.md4pdf

cp $1 $file
perl -i -p \
    -e "s|(\\\includegraphics)\{(.+?)\}|\\\vcenter{\\\hbox{\1\[height=8em\]{public/img/tikz/\2.pdf}}}|gm" \
    $file
