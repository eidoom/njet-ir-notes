# [njet-ir-notes](https://gitlab.com/eidoom/njet-ir-notes)

* Web render live [here](https://eidoom.gitlab.io/njet-ir-notes/).
* PDF render live [here](https://eidoom.gitlab.io/njet-ir-notes/njet-ir.pdf).

## Notes
* each `\includegraphics[height=...]{...}` must exist on its own line, due to regex rules!
* use [pandoc filter](https://pandoc.org/filters.html) instead of regex
* Consider https://phiresky.github.io/blog/2019/pandoc-url2cite/ or similar
* Consider [`mathjax`](https://github.com/mathjax/MathJax) for [better vertical alignment](https://tex.stackexchange.com/questions/417818/vertically-centering-symbols-and-graphics-inside-math-formulas)

## Build
* `make` makes the `pdf` and `html`
* `make public/njet-ir.pdf` produces a `pdf` in `public/`
* `make public/index.html` generates `html` webpage in `public/`
* `make njet-ir.tex` produces `njet-ir.tex`
* `make test` uses the `tex` to make a `pdf` in `test/` (different method than `make public/njet-ir.pdf`)
* `make public/img/tikz` just produces the diagrams in that directory

### Local autobuild 

#### Dependencies
```shell
sudo dnf install entr
```

#### PDF
```shell
make cont-pdf
```

#### webpage
```shell
make cont-web
```
Still have to refresh browser, though.

### Local Docker build
Could use the [eidoom/pandoc-physics](https://hub.docker.com/r/eidoom/pandoc-physics) image rather than installing dependencies locally. 
Enable this mode of operation with:
```shell
docker pull docker.io/eidoom/pandoc-physics:edge
export PANDOC='podman run -it --rm pandoc-physics:edge pandoc'
```
or using `podman` instead of `docker` on Fedora.
Untested.

### Remote
* webpage hosted by [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html) at https://eidoom.gitlab.io/njet-ir-notes/
* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) used to automatically regenerate webpage for new commits
* configured at [`.gitlab-ci.yml`](https://gitlab.com/eidoom/njet-ir-notes/-/blob/master/.gitlab-ci.yml)
* uses Docker image [eidoom/pandoc-physics](https://hub.docker.com/r/eidoom/pandoc-physics) ([source](https://gitlab.com/eidoom/pandoc-physics-image))

## Citations
* `bib` snippets from [inspirehep](https://inspirehep.net/)
* `csl` styling from [citation-style-language/styles](https://github.com/citation-style-language/styles).
    * [Project homepage](https://citationstyles.org/)
    * [Style search](https://www.zotero.org/styles)

## Dependencies
* `pandoc` - [docs](https://pandoc.org/MANUAL.html)
    * filter [`citeproc`](https://github.com/jgm/pandoc-citeproc/blob/master/man/pandoc-citeproc.1.md)
    * filter [`crossref`](http://lierdakil.github.io/pandoc-crossref/)
    * filter [`include-code`](https://github.com/owickstrom/pandoc-include-code)
        * `pandoc-types`
* `latex`
    * package `feynhand` - [docs](https://arxiv.org/pdf/1802.00689.pdf)
* `latexmk` - only for `make test`
* `pdf2svg` - only for `html`
* `katex` - only for `html`, installed by `make` - [docs](https://katex.org/docs/supported.html)

The following installation instructions are for Fedora 32.

### Latex
The lazy way to install [texlive](https://fedoraproject.org/wiki/Features/TeXLive):
```shell
sudo dnf install texlive-scheme-full
```

### latexmk
```shell
sudo dnf install latexmk
```

### pandoc
* You can install with the `dnf` package manager:
    ```shell
    sudo dnf install pandoc pandoc-citeproc
    ```
* However, this version is outdated; install the latest version (required for some used features) by manually installing the latest tarball [release](https://github.com/jgm/pandoc/releases) (includes `citeproc`):
    ```shell
    wget https://github.com/jgm/pandoc/releases/download/2.10/pandoc-2.10-linux-amd64.tar.gz
    tar -xvzf pandoc-2.10-linux-amd64.tar.gz --strip-components 1 -C ~/.local
    ```
* Alternatively, use the Haskell software tool `cabal` (`pandoc` build is slooow to build):
    ```shell
    sudo dnf install haskell-platform
    cabal update
    cabal install Cabal cabal-install
    cabal install pandoc pandoc-citeproc
    ```

### pandoc-crossref
* Release
    ```shell
    wget https://github.com/lierdakil/pandoc-crossref/releases/download/v0.3.7.0/pandoc-crossref-Linux-2.10.tar.xz
    tar -xvJf pandoc-crossref-Linux-2.10.tar.xz pandoc-crossref
    mv pandoc-crossref ~/.local/bin
    ```
* Cabal
    ```shell
    cabal install pandoc-crossref
    ```

<!-- ### pandoc-url2cite -->
<!-- ```shell -->
<!-- sudo npm install -g pandoc-url2cite -->
<!-- ``` -->

<!-- ### pandoc-types -->
<!-- * Cabal -->
<!--     ```shell -->
<!--     cabal install pandoc-types -->
<!--     ``` -->

### pandoc-include-code
<!-- * Release (usage fails due to old version of `pandoc-types`) -->
<!--     ```shell -->
<!--     wget https://github.com/owickstrom/pandoc-include-code/releases/download/v1.2.0.2/pandoc-include-code-linux-ghc8-pandoc-1-19.tar.gz -->
<!--     tar -xvzf pandoc-include-code-linux-ghc8-pandoc-1-19.tar.gz -->
<!--     mv pandoc-include-code ~/.local/bin -->
<!--     ``` -->
<!-- * Cabal (usage fails due to old version of `pandoc-types`: this uses 1.20, everything else on 1.21) -->
<!--     ```shell -->
<!--     cabal install pandoc-include-code -->
<!--     ``` -->
* Manual install
    ```shell
    git clone git@github.com:owickstrom/pandoc-include-code.git
    cd pandoc-include-code
    cabal configure
    cabal install
    ```
