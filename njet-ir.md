![amplitude](a)

# Notation

## Colour and helicity sums

Amplitudes are colour decomposed as
$$
\mathcal{A} = \sum_i \boldsymbol{c}_i A_i
$$ {#eq:colour-decomposition}
where

* the elements $\boldsymbol{c}_i A_i$ of the decomposition form a *colour basis*. Unlike the conventional use of the word, a colour basis may or may not be comprised of linearly-independent elements.
* these elements factorise into a kinematic contribution (describing momenta and spin) and a colour contribution.
* the $A_i$ are the *partial* or *colour-ordered amplitudes*, which are the kinematic parts of the colour decomposition.
* *primitive amplitudes* are a subset of the partial amplitudes for which the colour basis is linearly independent.
* the $\boldsymbol{c}_i$ are the *colour factors*, which are the colour parts of the colour decomposition. The boldface is to denote that they are tensors in colour space ($\mathrm{SU}(N_c)$, with $N_c=3$ for QCD), carrying indices in the (anti-)fundamental and adjoint respresentations for the colour charge of external (anti-)quarks and gluons respectively; the colour indices are suppressed for brevity.
* the operation of reconstructing an amplitude from the colour factors and partial amplitudes (*i.e.* performing [@eq:colour-decomposition]) is called a *colour sum* and referred to as *colour dressing*.
* an amplitude which is not colour decomposed may be described as *colour dressed* to distinguish it from a partial amplitude.

Helicity:

* *helicity amplitudes* are amplitudes with definite helicity on all external legs
* a *helicity-summed amplitude* is the sum over all possible helicity configurations
* *squared amplitude* is the modulus square of the amplitude. Since it is constructed from colour-decomposed factors, it involves a colour sum. May also involve a helicity sum.
* since all particles are massless in this treatment, the terms spin and helicity are used interchangeably

## Infrared factorisation

Amplitudes factorise in the infrared limit as
$$
\mathcal{A} \rightarrow \widetilde{\mathcal{A}} \otimes \mathcal{S}
$$
where 

* $\mathcal{A}$ is the full amplitude
* $\widetilde{\mathcal{A}}$ is the *reduced amplitude*
* $\otimes$ indicates summation over spin and colour states of correlated particles
* $\mathcal{S}$ is a universal singular function
* if there is no summation over correlated states, *i.e.* the $\otimes$ reduces to an ordinary multiplication $\times$, this is called an *exact factorisation*

## Infrared limits

### Collinear limit

For $i||j$, the kinematics are characterised by
$$
\mathrm{s}_{ij}\rightarrow 0.
$$

### Soft limit

$|p_j|\rightarrow0$, and, since gluons are massless, $E_j\rightarrow0$.

The kinematics are characterised by
$$
\mathrm{s}_{jl} \rightarrow 0 \quad \forall l\neq j.
$$

# Primary limits

## Single unresolved

### Double collinear {#sec:c2}

In most senses, the simplest QCD infrared limit is when two partons go collinear.
For an $n$-parton amplitude, there is a factorisation into an $(n-1)$-parton reduced amplitude and a three-legged *splitting amplitude*
$$
\mathcal{A}_n \rightarrow \widetilde{\mathcal{A}}_{n-1} \otimes \mathcal{P}_3.
$$

At the squared amplitude level, there is only a double spin correlation and the colour factorisation is exact.
This is why the limit enjoys such simplicity.

There are three possible double collinear limits in QCD: $c_{gg}, c_{qg}, c_{\bar{q}q}$.

In the double collinear limit, the $n$-particle partial helicity amplitude $A\coloneqq A(1^{h1},\ldots,i^{h_i},j^{h_j},\ldots,n^{h_n})$ can be expressed as the helicity sum
$$
c_{ij}(A) = 
\sum_{h\in\{-,+\}} \widetilde{A}_h P_{-h}
$$ {#eq:collinear-factorisation}
where 

* $\widetilde{A}_h\coloneqq\widetilde{A}(1^{h1},\ldots,(i-1)^{h_{i-1}},\rho^h,(j+1)^{h_{j+1}},\ldots,n^{h_n})$ are the $(n-1)$-particle reduced partial amplitudes.
* $P_h\coloneqq P(i^{h_i},\rho^{-h},j^{h_j})$ are the (kinematic) splitting amplitudes.
* with $\rho$ and $h$ denoting the correlated leg and its helicity (on the reduced amplitude) respectively.

To see colour factorisation at amplitude level, we individually colour dress (*c.f.* [@eq:colour-decomposition]) each element in [@eq:collinear-factorisation] as
$$
\begin{aligned}
\mathcal{A} &= \boldsymbol{c}_\mu A^\mu
&
\mathcal{\widetilde{A}}_h &= \boldsymbol{d}_\mu \widetilde{A}_h^\mu
&
\mathcal{P}_h &= \boldsymbol{b}_\mu P_h^\mu
\end{aligned}
$$
where

* $\mu$ sums over the partial amplitudes and colour factors in a colour basis.
* colour factors are in boldface to denote colour tensors.
* Einstein summation notation is employed (here and henceforth).

Consider the double-gluon collinear limit. Restoring explicit colour indices, we have
$$
c_{ij}(c_{\mu, f_1 \ldots f_{n_q}}^{a_1 \ldots a_i a_j \ldots a_{n_g}} A^{\mu}) = 
d_{\nu, f_1 \ldots f_{n_q}}^{a_1 \ldots a_{i-1} a_{\rho} a_{j+1} \ldots a_{n_g}} \widetilde{A}^{\nu}_h \,
b_{\tau}^{a_{i} a_{\rho} a_{j}} P^{\tau}_{-h}
$$ {#eq:colour-dressed-collinear-factorisation}
where

* the $a_i\in\{1,\ldots,{N_c}^2-1\}$ are adjoint colour indices, with $n_g$ external gluons in $A$.
* the $f_i\in\{1,\ldots,N_c\}$ fundamental colour indices , with $n_q$ external quarks in $A$.
* the colour structure of the splitting amplitude is trivial: the colour basis has a single element ($\tau\in\{1\}$). 
* there remains a sum over the helicity $h$. Having this spin correlation, kinematic factorisation is not exact.
* there is also a sum over the colour index $a_{\rho}$, so we see that colour factorisation is also not exact with a single correlation.

To summarise the double collinear limit at the amplitude level,
$$
c_{ij}(\mathcal{A}) = 
\widetilde{\mathcal{A}}_h \cdot \mathcal{P}_{-h}
$$ {#eq:collinear-amp-level}
where free colour indices are suppressed.

Pictorially, this looks like (drawing gluons in lieu of any parton)
$$
\includegraphics{a_c2}
\overset{i||j}{\longrightarrow}
\sum_h
\includegraphics{r_c2}
\otimes
\includegraphics{split_g2gg}.
$$

Turning to the squared helicity amplitude, after the colour sums we have
$$
\big\lvert c_{ij}(\mathcal{A}) \big\rvert^2 = 
\big(\widetilde{\mathcal{A}}_{h_1,f_1 \ldots f_{n_q}}^{a_1 \ldots a_{\rho} \ldots a_{n_g}}
\mathcal{P}_{-h_1}^{a_{i} a_{\rho} a_{j}}\big)^\dagger 
\widetilde{\mathcal{A}}_{h_2,f_1 \ldots f_{n_q}}^{a_1 \ldots a_{\sigma} \ldots a_{n_g}}
\mathcal{P}_{-h_2}^{a_{i} a_{\sigma} a_{j}}
$$ {#eq:collinear-square-gluon}

Consider
$$
\mathrm{colour}\big({\mathcal{P}_{-h_1}^{a_{i} a_{\rho} a_{j}}}^\dagger 
\mathcal{P}_{-h_2}^{a_{i} a_{\sigma} a_{j}}\big) =
f^{a_i a_{\rho} a_j} f^{a_i a_{\sigma} a_j} =
C_A \delta^{a_\rho a_\sigma}
$$ {#eq:delta-colour}
where $C_A$ is the adjoint Casimir.

So contracting the colour indices of the splitting amplitudes always results in a Kronecker delta over the collinear colour indices.
This is similarly true for the other double collinear limits: $c_{\bar{q}q}$ and $c_{qg}$.
This breaks the colour correlation that we observed at amplitude level to give exact factorisation of colour at the squared amplitude level.
The spin correlations remain.

Armed with this observation, [@eq:collinear-square-gluon] can be rearranged as
$$
\big\lvert c_{ij}(\mathcal{A}) \big\rvert^2 = 
({\widetilde{\mathcal{A}}_{h_1}}^\dagger\cdot\widetilde{\mathcal{A}}_{h_2}) 
({\mathcal{P}_{-h_1}}^\dagger\cdot\mathcal{P}_{-h_2})
$$ {#eq:collinear-square-two}
where the $\cdot$ denotes contraction of colour indices, allowing us to write an expression independent of the splitting amplitude parton flavours.

Consider the $2\times2$ matrix $\mathcal{P}_{h_1 h_2}$ with entries 
$$
\mathcal{P}_{h_1 h_2} \coloneqq {\mathcal{P}_{h_1}}^\dagger\cdot\mathcal{P}_{h_2}
$$ {#eq:spin-matrix}
and similarly for ${\widetilde{\mathcal{A}}}_{h_1 h_2}$.
We call these *spin matrices*.
The off-diagonal terms ($h_1\neq h_2$) are called *spin correlations* and make subdominant contributions to [@eq:collinear-square-two].
The squared amplitude can be constructed as a sum over entries of spin matrices
$$
\big\lvert c_{ij}(\mathcal{A}) \big\rvert^2 = 
{\widetilde{\mathcal{A}}}_{h_1 h_2}
\mathcal{P}_{-h_1 -h_2}
$$ {#eq:collinear-square}
This is written with explicit matrices in [App. @sec:collinear-matrices].

`NJet` constructs the spin matrices for this.
Amplitude classes have a method 
```cpp
born_spnmatrix(int i, int* h, std::complex* spn_matrix)
```
where the second and third parameters are pointers to C-style arrays.
This method populates the length four array `spn_matrix` as the flattened tree-level spin matrix for helicity configuration `h` with correlations on leg `i`.
The ordering is row-wise, so $\{++, +-, -+, --\}$ (*c.f.* [@eq:spin-matrix]).

An example implementation of [@eq:collinear-square] is in [App. @sec:njet-spin-matrices].

### Single soft

The next simplest limit is when a single parton goes soft.
This is only possible with a gluon, *i.e.* the limit $s_g$, as taking a gluon soft would result in a violation of quark number conservation in the reduced amplitude.
The full $n$-parton amplitude again factorises to an $(n-1)$-parton reduced amplitude and an *eikonal amplitude* with dependence on three legs of the full amplitude
$$
\mathcal{A}_n \rightarrow \widetilde{\mathcal{A}}_{n-1} \otimes \mathcal{S}_3.
$$

#### Kinematics

At the level of the partial amplitude, taking the $j^\mathrm{th}$ leg soft shows exact factorisation of the kinematics as
$$
\begin{aligned}
s_j \big( A(1^{h_1},&\ldots,j^{h_j},\ldots,n^{h_n}) \big) =\\
&R\big(1^{h_1},\ldots,(j-1)^{h_{j-1}},(j+1)^{h_{j+1}},\ldots,n^{h_n}\big)
\times
S_{h_j}\big(j-1,j,k\big)
\end{aligned}
$$ {#eq:soft-kinematic-factorisation}
where $k\in(\{1,\ldots,n\}/\{j-1,j\})$.
The $(j-1)^\mathrm{th}$ leg is the one from which the soft gluon radiates, so called the *radiating leg* or parent leg of the soft emission.
The $k^\mathrm{th}$ leg is the *correlated leg*, whose provenance is more evident at the level of the squared amplitude.

The eikonal amplitude depends only on the helicity of the soft leg, $h_j$, and the momenta of the soft leg, $p_j$, the radiating leg, $p_{j-1}$, and the correlated leg $p_k$.
At tree level, it takes the form
$$
\begin{aligned}
S_+(i,j,k)&=\frac{\langle i k \rangle}{\langle i j \rangle \langle j k \rangle}, 
\\
S_-(i,j,k)&=-\frac{[ i k ]}{[ i j ][ j k ]}.
\end{aligned}
$$ {#eq:eikonal}
These expressions are universal: they are independent of the flavour and helicity of partons $i$ and $k$.

With $p_j=\lambda_j q_j$, $\lambda_j\in \mathbb{R}$, this can be pictorially represented as
$$
\includegraphics{a_s1}
\;\overset{\lambda_j\rightarrow0}{\longrightarrow}\;
\includegraphics{r_s1}
\times
\includegraphics{soft_gg2g}
$$ {#eq:soft-partial-diagram}
where the circle vertex stands for the eikonal amplitude (@eq:eikonal).

No momenta mapping scheme is required to make the reduced phase space physical since in the soft limit we get this for free.



#### Amplitude level

We have found that taking a gluon soft leads to exact factorisation of the kinematics at the amplitude level.
However, no matter how soft the gluon is, it still carries the same colour charge.
Thus, the colour cannot be exactly factorised and colour correlations are produced.
In fact, to reproduce the full colour structure of an $n$-parton amplitude from the reduced and eikonal amplitudes, it is necessary to sum over emissions of the soft gluon from all the external legs $i\in\{1,\ldots,n-1\}$ of the reduced amplitude
$$
s_j \big( \mathcal{A} \big) = 
\sum_{i=1}^{n-1} \mathcal{A}^{(i)}
.
$$
To make this indexing of the legs of the reduced amplitude match with that of the full amplitude, we could take $j=n$.
This discussion will remain general.

Writing explicit colour indices using abstract index notation ($c_i$ are generic colour indices, *i.e.* we have not specified their representation), terms in this sum look like
$$
{\mathcal{A}^{(i)}}_{c_1 \ldots c_{i-1} c_{i+1} \ldots c_{\bar{k}} \ldots c_{n-1} c_j c_l} = 
\mathcal{R}_{c_1 \ldots c_i \ldots c_{\bar{k}} \ldots c_{n-1}} 
{\mathcal{S}}_{c_i c_j c_l}
$$ {#eq:cc-term}
with eikonal contributions
$$
\begin{aligned}
{\mathcal{S}}_{c_i c_j c_l} &=
T_{c_i c_j c_l} \, S^{(i)}_j
\\
S^{(i)}_j &=
\frac{{p_i} \cdot \varepsilon^{h_j}(p_j)}{p_i \cdot p_j}
\end{aligned}
$$ {#eq:naive-eikonal}
including a colour tensor $T_{c_i c_j c_l}$ and the polarisation vector of the soft gluon $\varepsilon_\mu^{h_j}(p_j)$.
The colour index:

* $c_i$ refers to the internal section of the radiating leg (summed over in [@eq:cc-term]), 
* $c_j$ to the soft emission, 
* $c_l$ is the extra index introduced on the external section of the emitting leg,
* and $c_{\bar{k}}$ is the correlated leg, which does not play a role at the amplitude level, but will at the squared amplitude level.

The flavour of leg $i$ determines the form of $T$:

* gluon: $f^{a_i a_j a_n}$
* quark: $t^{a_j}_{f_i f_n}$
* antiquark: $\bar{t}^{a_j}_{\bar{f}_i \bar{f}_n} = -t^{a_j}_{f_n f_i}$

Note that due to the simplicity of the single soft gluon eikonal colour structure, the colour "sum" contains only one term.
The colour-dressed reduced amplitude is, with colour factors $\boldsymbol{d}^a$,
$$
\mathcal{R}_{c_1 \ldots c_{n-1}} =
d_{c_1 \ldots c_{n-1}}^a \, R_a.
$$

Substituting these in,
$$
\begin{aligned}
{\mathcal{A}^{(i)}}_{c_1 \ldots c_{i-1} c_{i+1} \ldots c_{\bar{k}} \ldots c_{n-1} c_j c_l} =& \;
\, {\mathcal{C}^{(i)}}_{c_1 \ldots c_{i-1} c_{i+1} \ldots c_{\bar{k}} \ldots c_{n-1} c_j c_l} \,
S^{(i)}_j
\\
{\mathcal{C}^{(i)}}_{c_1 \ldots c_{i-1} c_{i+1} \ldots c_{\bar{k}} \ldots c_{n-1} c_j c_l} \coloneqq& \;
\, d^a_{c_1 \ldots c_i \ldots c_{\bar{k}} \ldots c_{n-1}} 
\, T_{c_i c_j c_l} 
\, R_a
\end{aligned}
$$
where $\mathcal{C}^{(i)}$ is the *colour-correlated amplitude*.

Bringing this together, at the amplitude level we have
$$
s_j \big( \mathcal{A} \big) = 
\sum_{i} {\mathcal{C}^{(i)}} \, S^{(i)}_j
$$ {#eq:soft-amp-level}
where colour indices are implicit.
There are no spin correlations, but the state is colour correlated.
The degree of colour correlation grows with the multiplicity $n$ of the full QCD amplitude.

Diagramatically, this can be expressed as
$$
\includegraphics{ha_s1}
\;\overset{\lambda_j\rightarrow0}{\longrightarrow}\;
\sum_{i}
\includegraphics{hr_s1}
\otimes
\includegraphics{hs1}
$$
where the filled circle indicates the emission of a soft gluon with momentum $p_j$ from the $i^{\mathrm{th}}$ leg of the reduced amplitude as the expression of [@eq:naive-eikonal], which we denote the eikonal amplitude in *naive form*.
This is related to the eikonal amplitude of [@eq:eikonal] (drawn in [@eq:soft-partial-diagram]), which we call the *Weyl form*, in that they are both factorisations of the squared eikonal amplitude expression ([@eq:eikonal-sq]) into, in some sense, a square root and its conjugate.
We shall see how the eikonal amplitude in Weyl form arises shortly.

#### Squared amplitude level

For the squared helicity amplitude,
$$
\big\lvert s_j(\mathcal{A}) \big\rvert^2 =
\sum_{i,k}
\mathrm{A}^{(i,k)}
$$ {#eq:soft-square-zero}
with
$$
\mathrm{A}^{(i,k)} =
\mathrm{C}^{(i,k)} \, \mathrm{S}_j^{(i,k)}
$$ {#eq:soft-square-first}
and
$$
\begin{aligned}
\mathrm{A}^{(i,k)} \coloneqq& \;
{\mathcal{A}^{(i)}}^\dagger\cdot\mathcal{A}^{(k)}
\\
\mathrm{C}^{(i,k)} \coloneqq& \;
{\mathcal{C}^{(i)}}^\dagger\cdot{\mathcal{C}^{(k)}}
\\
\mathrm{S}_j^{(i,k)} \coloneqq& \;
{S^{(i)}_j}^\dagger S^{(k)}_j
\end{aligned}
$$

Consider the *eikonal matrix*
$$
\mathrm{S}_j^{(i,k)} =
\frac
{{p_i}^\mu \, {p_k}^\nu \, {\varepsilon_\mu^{h_j}(p_j)}^\dagger \, {\varepsilon_\nu^{h_j}(p_j)}}
{(p_i \cdot p_j)(p_k \cdot p_j)}.
$$

Contracting the helicity index over the polarisation tensors yields the gluon polarisation tensor via the completeness relation
$$
d_{\mu\nu}(p_j) = - \eta_{\mu\nu} + \ldots
$$
where the omitted terms can be neglected as they cancel out in [@eq:soft-square-first].
Thus,
$$
\mathrm{S}_j^{(i,k)} =
-\frac
{{p_i}\cdot {p_k}}
{(p_i \cdot p_j)(p_k \cdot p_j)} =
-2 \frac{s_{ik}}{s_{ij} s_{jk}}
$$ {#eq:eikonal-sq}
which can be constructed from [@eq:eikonal] as
$$
\mathrm{S}_j^{(i,k)} = -2 \, |S_{h_j}(i,j,k)|^2
$$
giving the connection from the naive form to the Weyl form of the eikonal amplitude.

The *colour correlation matrix* $\mathrm{C}^{(i,k)}$ is constructed from the reduced partial amplitudes and the colour correlations as
$$
\mathrm{C}^{(i,k)} =
{R_a}^\dagger \, {\mathrm{D}^{(i,k)}}^{ab} \, R_b
$$
with the colour matrices
$$
{\mathrm{D}^{(i,k)}}^{ab} \coloneqq
d^a_{c_1 \ldots c_i \ldots c_{\bar{k}} \ldots c_{n-1}} 
\, T_{c_i c_j c_{\bar{i}}} \, T_{c_k c_j c_{\bar{k}}} \, 
d^b_{c_1 \ldots c_k \ldots c_{\bar{i}} \ldots c_{n-1}}.
$$
Now we see the origin of the correlated legs, $\bar{i}$ and $\bar{k}$: they are the legs which attach to the radiating leg of the conjugate expression (hence the notation).

Expressed in diagrams, 
$$
\mathrm{D}^{(i,k)}=
\includegraphics{sa_s1}
$$

Construction of this colour matrix can be computationally intensive at high multiplicities.

Notice from [@eq:eikonal-sq] that the diagonal of the eikonal matrix vanishes by masslessness
$$
\mathrm{S}_j^{(i,i)}=0.
$$
Also, the colour correlation matrix is symmetric
$$
\mathrm{C}^{(i,k)}=\mathrm{C}^{(k,i)}.
$$
Thus, [@eq:soft-square-zero] can be optimised as
$$
\big\lvert s_j(\mathcal{A}) \big\rvert^2 =
2 \sum_{k>i}
\mathrm{C}^{(i,k)} \, \mathrm{S}_j^{(i,k)}.
$$ {#eq:soft-square}

#### Soft current

The literature generally treats the colour with a notation described in Sec. 3.1 of Ref.[@catani:1999].
This notation uses an abstract colour charge $\boldsymbol{T}_i$ which comes with the emission of a gluon from the $i^\mathrm{th}$ parton.
The limit expression is constructed from an $n$-parton reduced matrix element and an eikonal current
$$
\boldsymbol{J}^{\mu}(q) = \sum_{i=1}^{n} \boldsymbol{T}_i \frac{{p_i}^{\mu}}{p_i \cdot q}
$$
The eikonal amplitudes ([@eq:eikonal]) can be obtained from the kinematic part of the eikonal current by contracting with a gluon polarisation vector of the appropriate helicity and replacing Minkowski products with the helicity bracket notation
$$
\begin{aligned}
\varepsilon_{\mu}^-(q,r) & = - \frac{[ r \sigma_\mu q \rangle}{\sqrt{2} [ r q ]} &
\varepsilon_{\mu}^+(q,r) & = \frac{\langle r \overline{\sigma}_\mu q ]}{\sqrt{2} \langle r q \rangle} &
p_i \cdot p_j & = \frac{1}{2} s_{ij} = \frac{1}{2} \langle i j \rangle [j i]
\end{aligned}
$$
The origin of the correlated leg is seen here as the arbitrary reference momentum $r$ introduced to express the polarisation vector.

#### Computation

`NJet` provides the elements of the colour correlation matrix and eikonal matrix to perform [@eq:soft-square] as shown in [App. @sec:njet-colour-correlations].
The method
```cpp
born_ccij(int* h, int i, int k)
```
is called on the class of the reduced amplitude to generate $\mathrm{C}^{(i,k)}$.
Currently, $\mathrm{S}_j^{(i,k)}$ is constructed from the eikonal amplitude class by setting the appropriate momenta with the method 
```cpp
setMomenta(MOM<T>* momenta, MOM<T> reference_momentum)
```
where `MOM<T>` is a templated user-defined type in `NJet` for four-momenta with entries of type `T` (generally `double`) and `momenta` has ordering as in the arguments of $S$ in [@eq:eikonal], then calling the method
```cpp
born(int* h)
```
Possibly I should change the soft class to reflect spin dependence on soft leg only and to add a method like `born_ijk(int h, int i, int j, int k)` that sets the momenta (would need to `setMomenta` with the full phase space once at start).

## Double unresolved

### Triple collinear

$$
c_{ggg}, c_{qgg}, c_{\bar{q}qg}, ...
$$

$$
\mathcal{A}_n \rightarrow \widetilde{\mathcal{A}}_{n-2} \otimes \mathcal{P}_4
$$

$$
\includegraphics{a_c3}
\overset{i||j||k}{\longrightarrow}
\sum_h
\includegraphics{r_c2}
\otimes
\includegraphics{split_g2ggg}
$$

Treatment is very similar to that of the [double collinear limit](#sec:c2).

### Double soft

$$
s_{gg}
$$

$$
\mathcal{A}_n \rightarrow \widetilde{\mathcal{A}}_{n-2} \otimes \mathcal{S}_2
$$

notation? iterated $\mathcal{S}_{1,1}$ vs correlated $\mathcal{S}_2$?

## Loop level

$$
\mathcal{A}^{1\mathrm{L}} \rightarrow \widetilde{\mathcal{A}}^{1\mathrm{L}} \otimes \mathcal{S}^{0\mathrm{L}} + \widetilde{\mathcal{A}}^{0\mathrm{L}} \otimes \mathcal{S}^{1\mathrm{L}}
$$

$$
\begin{aligned}
\includegraphics{a_c2_1l}
\overset{i||j}{\longrightarrow}
&\sum_h
\includegraphics{r_c2_1l}
\otimes
\includegraphics{split_g2gg}\\
+
&\sum_h
\includegraphics{r_c2}
\otimes
\includegraphics{split_g2gg_1l}
\end{aligned}
$$

# Iterated and sub limits

## Overlapping regions

## Regularisation schemes

$$
\mathrm{I} = \int_{x_1}^{x_2} \mathrm{d}x\, f(x)
$$
Regulate by writing $\mathrm{I}$ as the sum of the divergent part, $\mathrm{I}_1$, and the finite part, $\mathrm{I}_2$.

### Slicing

$$
\mathrm{I} = \int_{x_1}^{\delta} \mathrm{d}x\, f(x) + \int_{\delta}^{x_2} \mathrm{d}x\, f(x)
$$

### Subtraction

$$
\mathrm{I} = \int_{x_1}^{x_2} \mathrm{d}x\, s(x) f(x) + \int_{x_1}^{x_2} \mathrm{d}x\, \left(1-s(x)\right) f(x)
$$

## Iterated limits

Further factorisation of the reduced amplitude, by taking multiple primary limits.
Limits are not correlated.

## Sub limits

Further factorisation of the singular function, by taking limits of the singular function.
Limits are correlated.

## Overlapping limits

?

A limit of the singular function which does not factorise.

# QCD limits in perturbation theory

## NLO

### Single unresolved

$$
\mathcal{A}^{(1)} \rightarrow \widetilde{\mathcal{A}}^{(0)} \otimes \mathcal{S}^{(0)}
$$

primary

$\mathrm{IR}^{(0)}\ni$

$$
s_{g}, c_{gg}, c_{qg}, c_{\bar{q}q}
$$

sub

$\mathrm{IR}^{(0)}\otimes\mathrm{IR}^{(0)}\ni$ ($\otimes$ denotes correlated limits)

$$
s_{g}c_{gg}, s_{g}c_{qg}
$$

## NNLO

### Double unresolved

$$
\mathcal{A}^{(2)} \rightarrow \widetilde{\mathcal{A}}^{(0)} \otimes \mathcal{S}^{(1)}
$$

primary

$\mathrm{IR}^{(1)}\ni$

soft
$$
s_{gg}, s_{\bar{q}q}
$$

collinear
$$
c_{ggg}, c_{qgg}, c_{\bar{q}qg}, ...
$$

iterated

$\mathrm{IR}^{(0)}\times\mathrm{IR}^{(0)}\ni$ ($\times$ denotes uncorrelated limits)

$$
s_{g}c_{gg}, s_{g}s_{gg}, c_{gg}c_{gg}, ...
$$

sub

$\mathrm{IR}^{(0)}\otimes\mathrm{IR}^{(1)}\ni$

$$
s_{g}c_{ggg}, s_{g}s_{gg}, c_{gg}s_{gg}, ...
$$

$\mathrm{IR}^{(1)}\otimes\mathrm{IR}^{(1)}\ni$

$$
s_{gg}c_{ggg}, ...
$$

### Single unresolved

$$
\mathcal{A}^{(2)} \rightarrow \widetilde{\mathcal{A}}^{(1)} \otimes \mathcal{S}^{(0)}
$$

Limits as for [NLO](#nlo).

# Test

Word[@catani:1999].
Word[@frixione:2004].
Word[@herzog:2018].

<!-- [@catani:1999]: https://arxiv.org/abs/hep-ph/9908523 -->
<!-- [@frixione:2004]: https://arxiv.org/abs/hep-ph/0411399 -->
<!-- [@herzog:2018]: https://arxiv.org/abs/1804.07949 -->

<!-- # Bibliography {.unnumbered .unlisted } -->
<!-- ::: { #refs } -->
<!-- ::: -->

\appendix

# Appexdix: collinear limit in matrix notation {#sec:collinear-matrices}

Colour summing [@eq:colour-dressed-collinear-factorisation] and writing as a vector product in helicity space, we obtain
$$
c_{ij}(\mathcal{A}) = 
\begin{pmatrix}
\widetilde{\mathcal{A}}_- & \widetilde{\mathcal{A}}_+ 
\end{pmatrix}
\begin{pmatrix}
\mathcal{P}_{+} \\
\mathcal{P}_{-}
\end{pmatrix}.
$$
Note that the quantities on the righthand side are colour tensors with suppressed colour indices to which the Kronecker delta of [@eq:delta-colour] has been preemptively applied.

The spin matrices (*c.f.* [@eq:spin-matrix]) become
$$
\begin{aligned}
\mathrm{M}_{\mathcal{P}} &=
\begin{pmatrix}
   \mathcal{P}_{++} & \mathcal{P}_{+-} \\
   \mathcal{P}_{-+} & \mathcal{P}_{--}
\end{pmatrix}
&
\mathrm{M}_{\widetilde{\mathcal{A}}} &= 
\begin{pmatrix}
   \widetilde{\mathcal{A}}_{++} & \widetilde{\mathcal{A}}_{+-} \\
   \widetilde{\mathcal{A}}_{-+} & \widetilde{\mathcal{A}}_{--}
\end{pmatrix}
\end{aligned}.
$$

To write the amplitude in the limit in terms of these spin matrices, we first introduce the *reverse matrix* operation denoted by $^\leftarrow$ and defined by
$$
\mathrm{M}^{\leftarrow} \coloneqq \mathrm{J}_2 \, \mathrm{M} \, \mathrm{J}_2
$$
where $\mathrm{J}_2$ is the $2\times 2$ exchange matrix,
$$
J_2 =
\begin{pmatrix}
   0 & 1 \\
   1 & 0
\end{pmatrix},
$$
such that
$$
\mathrm{M}_{\mathcal{P}}^{\leftarrow} =
\begin{pmatrix}
   \mathcal{P}_{--} & \mathcal{P}_{-+} \\
   \mathcal{P}_{+-} & \mathcal{P}_{++}
\end{pmatrix}.
$$
Then, [@eq:collinear-square] can be rewritten as
$$
\big\lvert c_{ij}(\mathcal{A}) \big\rvert^2 = 
\mathrm{sum} \left( \mathrm{M}_{\widetilde{\mathcal{A}}} \odot \mathrm{M}_{\mathcal{P}}^{\leftarrow} \right)
$$
where $\mathrm{sum}$ takes the sum of the entries of a matrix, and $\odot$ is the matrix Hadamard product.

# Appendix: spin matrices in `NJet` {#sec:njet-spin-matrices}

We present an example implementation of [@eq:collinear-square] in `C++` using `NJet` for a collinear limit of the five-gluon tree-level helicity amplitude
$$
\mathcal{A}_{5g}(1^+,2^+,3^+,4^-,5^-) \overset{3||4}{\longrightarrow}
\mathcal{A}_{4g}(1^+,2^+,\mathrm{P}^h,5^-) \otimes 
\mathcal{P}_{g\rightarrow gg}(\mathrm{P}^{-h};3^+,4^-)
$$ 
where $\otimes$ represents [@eq:collinear-amp-level].
We test the modulus square of this helicity amplitude factorisation.
The code is as follows.
Recall that `C++` is zero-indexed.
``` {.cpp include=code-examples/c2_hel_amp_2.cpp}
```

This runs to give the result
```shell
|s34/s12|=1.3333346667433485e-06
lim/amp  =1.0007914576107262e+00
```
which confirms that the factorisation is correct in this configuration.
The reduced amplitude momenta were calculated using the Catani-Seymour momentum mapping scheme.

# Appendix: colour correlations in `NJet` {#sec:njet-colour-correlations}

We present an example implementation of [@eq:soft-square] in `C++` using `NJet` for a soft limit of the five-gluon tree-level helicity amplitude
$$
\mathcal{A}_{5g}(1^+,2^+,3^-,4^-,5^-) \overset{p_4\rightarrow\boldsymbol{0}}{\longrightarrow}
\mathcal{A}_{4g}(1^+,2^+,3^-,5^-) \otimes 
\mathcal{S}_{4}
$$ 
where $\otimes$ represents [@eq:soft-amp-level].
We test the modulus square of this helicity amplitude factorisation.
The code is as follows.
Recall that `C++` is zero-indexed.
``` {.cpp include=code-examples/s1_hel_amp_2.cpp}
```

This runs to give the result
```shell
|E4/E1|   = 9.9999999999544897e-06
lim2/amp2 = 9.9996000059999579e-01
```
validating the factorisation in this configuration.
