#include <array>
#include <iostream>

struct mom4 : std::array<double, 4> {
    mom4(const double p0, const double p1, const double p2, const double p3);
    mom4(const double px);
    mom4();

    mom4 operator-() const;
    mom4& operator*=(const double& c);
    mom4& operator+=(const double& c);
    mom4& operator+=(const mom4& o);
    mom4& operator-=(const mom4& o);

    double mass2() const;
};

mom4 operator*(mom4 m, const double& c);
mom4 operator*(const double& c, mom4 m);
double operator*(const mom4& m, const mom4& o);
mom4 operator+(mom4 m, const double& c);
mom4 operator+(const double& c, mom4 m);
mom4 operator+(mom4 m, const mom4& o);
mom4 operator-(mom4 m, const mom4& o);
std::ostream& operator<<(std::ostream& stream, const mom4& m);
