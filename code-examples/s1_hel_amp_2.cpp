#include <algorithm>
#include <array>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>

#include "analytic/0q4g-analytic.h"
#include "analytic/0q5g-analytic.h"
#include "ir/soft_gg2g-analytic.h"
#include "ngluon2/Mom.h"

int main()
{
    std::cout << std::setprecision(16) << std::scientific;

    // 5-point phase space with p4 soft (unity index)
    const std::array<MOM<double>, 5> full_mom { {
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, -5.0000000000000000e-01 },
        { -5.0000000000000000e-01, 0.0000000000000000e+00, 0.0000000000000000e+00, 5.0000000000000000e-01 },
        { 4.9999949999999999e-01, 4.3301226887951738e-01, 0.0000000000000000e+00, 2.4999975000000005e-01 },
        { 4.9999999999772449e-06, -2.2505887718767004e-06, 1.7633487034433740e-06, -4.1018839000804920e-06 },
        { 4.9999550000000009e-01, -4.3301001829074548e-01, -1.7633487034433740e-06, -2.4999564811609998e-01 },
    } };

    // reduced phase space is just the full phase space without the soft leg
    std::array<MOM<double>, 4> reduced_mom {};
    std::copy_n(full_mom.cbegin(), 3, reduced_mom.begin());
    reduced_mom[3] = full_mom[4];

    const double scale { 1. };
    Amp0q5g_a<double> full_amp(scale);
    Amp0q4g_a<double> reduced_amp(scale);
    Softgg2g_a<double> soft_amp(scale);

    full_amp.setMomenta(full_mom.data());
    reduced_amp.setMomenta(reduced_mom.data());

    const std::array<int, 5> full_hels { +1, +1, -1, -1, -1 };
    const std::array<int, 4> reduced_hels { +1, +1, -1, -1 };
    // note that helicities of correlated legs in the eikonal amplitude are
    // the opposite of the corresponding legs on the reduced amplitude
    // NB the eikonal amplitude is actually independent of the helicities of these legs
    const std::array<int, 3> soft_hels { +1, -1, +1 };

    // perform colour sum
    double lim {};
    int ref_index {};
    for (int i { 0 }; i < 4; ++i) {
        for (int j { i + 1 }; j < 4; ++j) {
            // ensure reference momentum is not the same as another momentum in use
            // to avoid division by zero from brackets in the denominator of the form < i i >
            while ((ref_index == i) || (ref_index == j)) {
                ref_index = (ref_index + 1) % 4;
            }

            // set momenta for eikonal amplitude
            // the second argument is the reference momentum
            soft_amp.setMomenta({ reduced_mom[i], full_mom[3], reduced_mom[j] }, reduced_mom[ref_index]);

            const double soft_val { soft_amp.born(soft_hels.data()) },
                cc_val { reduced_amp.born_ccij(reduced_hels.data(), i, j) };

            lim += cc_val * soft_val;
        }
    }

    // we summed over upper triangle of colour correlation matrix
    // diagonals receive no contribution because eikonal kinematic factor is zero due to antisymmetry of brackets
    // matrix is symmetric, so add lower triangle by doubling result
    lim *= 2.;

    const double amp_val { full_amp.born(full_hels.data()) };

    std::cout
        << '\n'
        << std::setw(20) << "|E4^2/s12| = " << std::setw(23) << std::abs(pow(full_mom[3].x0, 2) / dot(full_mom[0], full_mom[1])) << '\n'
        << std::setw(20) << "lim2/amp2 = " << std::setw(23) << lim / amp_val << '\n'
        << std::setw(20) << "|lim2-amp2|/amp2 = " << std::setw(23) << std::abs(lim - amp_val) / amp_val << '\n'
        << '\n';
}
