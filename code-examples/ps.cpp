#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <numbers>
#include <numeric>
#include <tuple>

#include "mom4.hpp"

template <std::size_t mul>
void test_massless(const std::array<mom4, mul>& momenta)
{
    double zero { 1e-10 };
    std::for_each(momenta.cbegin(), momenta.cend(), [zero](const mom4& m) { assert(std::abs(m.mass2()) < zero); });
}

template <std::size_t mul>
void test_mom_cons(const std::array<mom4, mul>& momenta)
{
    double zero { 1e-10 };
    mom4 sum { std::accumulate(momenta.cbegin(), momenta.cend(), mom4(0.)) };
    std::for_each(sum.cbegin(), sum.cend(), [zero](double x) { assert(x < zero); });
}

std::array<mom4, 5> p5_c2(const double y1, const double y2, const double theta, const double alpha, const double sqrtS, int r)
{
    assert(0. <= y1);
    assert(0. <= y2);
    assert(y1 + y2 <= 1.);
    assert(r >= 0);

    r %= 5;

    const double x1 { 1. - y1 };
    const double x2 { 1. - y2 };

    // cos(beta)
    const double cb { 1. + 2. * (1. - x1 - x2) / x1 / x2 };
    assert((-1. <= cb) && (cb <= 1.));
    // sin(beta)
    const double sb { std::sqrt(1. - std::pow(cb, 2.)) };

    const double ct { std::cos(theta) };
    const double st { std::sin(theta) };

    const double ca { std::cos(alpha) };
    const double sa { std::sin(alpha) };

    std::array<mom4, 5> momenta { {
        sqrtS / 2. * mom4(-1., 0., 0., -1.),
        sqrtS / 2. * mom4(-1., 0., 0., 1.),
        x1 * sqrtS / 2. * mom4(1., st, 0., ct),
        x2 * sqrtS / 2. * mom4(1., ca * ct * sb + cb * st, sa * sb, cb * ct - ca * sb * st),
        mom4(),
    } };

    momenta[4] = -std::accumulate(momenta.cbegin(), momenta.cend() - 1, mom4(0.));

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

    test_massless(momenta);
    test_mom_cons(momenta);

    return momenta;
}

std::tuple<const std::array<mom4, 4>, const std::array<mom4, 3>> map2(const int o1, const std::array<mom4, 5>& mom)
{
    const int o2 { (o1 + 1) % 5 };
    const mom4 p1(mom[o1]);
    const mom4 p2(mom[o2]);
    const mom4 p3(mom[(o1 + 2) % 5]);
    const mom4 p12(p1 + p2);
    const double x12_3((p1 * p2) / (p12 * p3));
    const mom4 p12t(p12 - (x12_3 * p3));
    const mom4 p3t((1. + x12_3) * p3);
    const std::array<mom4, 3> split_mom { -p12t, p1, p2 };
    std::array<mom4, 4> reduced_mom;
    std::copy_n(mom.cbegin(), o1, reduced_mom.begin());
    reduced_mom[o1] = p12t;
    reduced_mom[o2] = p3t;
    if (o1 + 3 < 5) {
        std::copy(mom.cbegin() + o1 + 3, mom.cend(), reduced_mom.begin() + o1 + 2);
    }
    test_massless(reduced_mom);
    test_mom_cons(reduced_mom);
    test_massless(split_mom);
    return std::make_tuple(reduced_mom, split_mom);
}

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout << '\n';

    std::cout << "# double collinear\n\n";

    const std::array<mom4, 5> moms { p5_c2(1e-6, 0.4, std::numbers::pi / 3., std::numbers::pi / 5., 1., 1) };

    for (int i { 0 }; i < 5; ++i) {
        for (int j { i + 1 }; j < 5; ++j) {
            const double s { 2. * (moms[i] * moms[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << '\n';
        }
    }
    std::cout << '\n';

    // std::array<mom4, 4> red_moms;
    // std::array<mom4, 3> spl_moms;
    // std::tie(red_moms, spl_moms) = map2(2, moms);
    auto [red_moms, spl_moms] = map2(2, moms);

    std::cout << "const std::array<MOM<double>, 5> full_mom { {\n";
    for (const mom4& m : moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << "const std::array<MOM<double>, 4> reduced_mom { {\n";
    for (const mom4& m : red_moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << "const std::array<MOM<double>, 3> splitting_mom { {\n";
    for (const mom4& m : spl_moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << "# single soft\n\n";

    const std::array<mom4, 5> soft_moms { p5_c2(1e-6, 1.-1e-5, std::numbers::pi / 3., std::numbers::pi / 5., 1., 0) };

    for (int i { 0 }; i < 5; ++i) {
        for (int j { i + 1 }; j < 5; ++j) {
            const double s { 2. * (soft_moms[i] * soft_moms[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << '\n';
        }
    }
    std::cout << '\n';

    std::cout << "const std::array<MOM<double>, 3> full_mom { {\n";
    for (const mom4& m : soft_moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << '\n';
}
